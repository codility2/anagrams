import pytest
from main import count_anagrams

params = [
 (0, ""),
 (0, "double ontrange"),
 (0, "a b"),
 (0, "a a"),
 (2, "aa aa odg dog gdo"),
 (1, "a c b c run urn urn"),
 (2, "slap alps laps"),
 (3, "slap alps laps cal lac"),
 (3, "slap cal alps laps lac"),
 (2, "slap alps laps lac"),
 (1, "ok do ko"),
 (2, "ok do ko od"),
 (4, "ok bla do ko bal od lab"),
 ]

@pytest.mark.parametrize("expected, string", params)
def test_count_anagrams(string, expected):
    assert expected == count_anagrams(string)
