
def find_anagrams(words) -> dict:
    from collections import defaultdict
    word_id = lambda w: "".join(sorted(w))
    word_index = defaultdict(lambda:[])
    for w in words:
        word_index[word_id(w)].append(w)
    return [v for _,v in word_index.items() if len(v) > 1]

def count_anagrams(string: str) -> int:
    words = set([w for w in string.split() if len(w) > 1])
    anagrams = find_anagrams(words)
    return sum([len(a) for a in anagrams]) - len(anagrams)
